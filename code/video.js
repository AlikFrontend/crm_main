import { createHTMLElement, createEditProductInput } from "./functions.js";
import { modalClose, modalSave } from "./var.js";
import { showModalEvent, hideModalEvent } from "./events.js";




//Вивід на сторінку позицій меню для ресторану
function showVideoMenu(arr = []) {
    //Знайшли tbody для виводу інформації по позиціям 
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = ""

    arr.forEach(function ({ name, url, date, id }, i) {
        //#	Назва	Дата публікації	Посилання	Редагувати	Видалити
        const tr = createHTMLElement("tr"); // за допомогою ф-ї створюємо tr
        const element = [
            createHTMLElement("td", undefined, i + 1), // номер по черзі (undefined - клас)
            createHTMLElement("td", undefined, name),
            createHTMLElement("td", undefined, date),
            createHTMLElement("td", undefined, url),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#9998;</span>`, undefined, editProductVideEvent),// "&#9998;" - значок редагування // editProductStoreEvent - подія при нажатті // data-key="${id} - для ідентиікації об'єкта що потрібно змінювати (cтатус)
            createHTMLElement("td", undefined, `<span data-del="${id}" class='icon red'>&#10006;</span>`, undefined, delVideo),// "&#10006;" - значок  видалення

        ]
        tbody.append(tr);// додаємо tr у кінець
        tr.append(...element)// додаємо td у кінець tr
    })
}

if (localStorage.video) {

    showVideoMenu(JSON.parse(localStorage.video)); //якщо є база даних ресторану, тоді передаємо дані з даної БД.
}

// Змінюємо продукт з БД
function editProductVideEvent(e) {

    if (!e.target.tagName === "SPAN") return; // відслідковуємо елемент на якому буде виконуватися клік
    showModalEvent();// відкриваємо модальне вікно

    const span = e.target;
    const videoBD = JSON.parse(localStorage.video);

    const modalWindow = document.querySelector(".modal");// шукаємо модальне вікно
    const modalBody = createHTMLElement("div", "modal-body");  // створюємо розмітку модального вікна
    modalWindow.append(modalBody);

    // Робота з кнопками 
    const btns = createHTMLElement("div", "btns");

    modalSave.addEventListener("click", () => {

        newVideoSaveInfo(modalBody, rez)
        hideModalEvent() // закриваємо модальне вікно після збереження
        modalBody.remove()

        showVideoMenu(JSON.parse(localStorage.video)) // перегружаємо дані на сторінці після збереження

    });// зберігаємо зміни

    modalClose.addEventListener("click", () => {
        hideModalEvent()
        modalBody.remove()
    });// відміняємо зміни

    btns.append(modalSave, modalClose);
    modalWindow.append(btns)

    //Визначення обєкта для редагування
    const rez = videoBD.find((a) => {
        return span.dataset.key === a.id
    }); // rez відповідає за об'єкт в якому span, що нажімається,  має вказаний id
    const data = Object.entries(rez);// перебирання об'єкта 

    // Редагування позиції
    const inputsElemets = data.map(([props, value]) => {
        return createEditProductInput(props, value) // функція для відображення модального вікна для редагування
    })
    modalBody.append(...inputsElemets)
}
function newVideoSaveInfo(newObj, oldObj) { // нове збереження даних після редагування
    const inputs = newObj.querySelectorAll("input");

    const obj = { // параметри які не змінюються
        id: oldObj.id,
        date: oldObj.date
    }

    inputs.forEach(input => {// записуємо параметри які ми змінюємо
        switch (input.key) {
            case "name": obj.name = input.value;
                return
            case "poster": obj.poster = input.value;
                return
            case "description": obj.description = input.value;
                return
            case "keywords": obj.keywords = input.value.split(",");
                return
            case "url": obj.url = input.value;
                return

        }
    })


    const videoBD = JSON.parse(localStorage.video);
    videoBD.splice(videoBD.findIndex(el => el.id === oldObj.id), 1, obj);// findIndex - з якого удаляти, 1 - скільки удаляти, obj - на що заміняти
    localStorage.video = JSON.stringify(videoBD);
}

function delVideo(e) {
    const arrBD = JSON.parse(localStorage.video)
    const itemArrDel = arrBD.find((a) => {
        return e.target.dataset.del === a.id

    });
    arrBD.splice(arrBD.findIndex(el => el.id === itemArrDel.id), 1);
    localStorage.video = JSON.stringify(arrBD);
    console.log(itemArrDel.id);
    showVideoMenu(arrBD)
}


