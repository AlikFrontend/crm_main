import { createHTMLElement, createEditProductInput } from "./functions.js";
import { modalClose, modalSave } from "./var.js";
import { showModalEvent, hideModalEvent } from "./events.js";




//Вивід на сторінку позицій меню для ресторану
function showRestoranMenu(arr = []) {
    //Знайшли tbody для виводу інформації по позиціям 
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = ""

    arr.forEach(function ({ productName, productQuantity, price, status, date, id }, i) {
        //Назва	Залишок	Ціна	Редагувати	Статус	Дата додавання	Видалити
        const tr = createHTMLElement("tr"); // за допомогою ф-ї створюємо tr
        const element = [
            createHTMLElement("td", undefined, i + 1), // номер по черзі (undefined - клас)
            createHTMLElement("td", undefined, productName),
            createHTMLElement("td", undefined, productQuantity),
            createHTMLElement("td", undefined, price),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#9998;</span>`, undefined, editProductRestEvent),// "&#9998;" - значок редагування // editProductStoreEvent - подія при нажатті // data-key="${id} - для ідентиікації об'єкта що потрібно змінювати (cтатус)
            createHTMLElement("td", undefined, status ? "<span class='icon green'>&#10004;</span>" : "<span class='icon red'>&#10008;</span>"), // якщо є стопліст, то "&#10004;"- якщо немає - то "&#10008;"
            createHTMLElement("td", undefined, date),
            createHTMLElement("td", undefined, `<span data-del="${id}" class='icon red'>&#10006;</span>`, undefined, delRest),// "&#10006;" - значок  видалення

        ]
        tbody.append(tr);// додаємо tr у кінець
        tr.append(...element)// додаємо td у кінець tr
    })
}

if (localStorage.restorationBD) {
    showRestoranMenu(JSON.parse(localStorage.restorationBD)); //якщо є база даних ресторану, тоді передаємо дані з даної БД.
}

// Змінюємо продукт з БД
function editProductRestEvent(e) {
    if (!e.target.tagName === "SPAN") return; // відслідковуємо елемент на якому буде виконуватися клік
    showModalEvent();// відкриваємо модальне вікно

    const span = e.target;
    const restBD = JSON.parse(localStorage.restorationBD);

    const modalWindow = document.querySelector(".modal");// шукаємо модальне вікно
    const modalBody = createHTMLElement("div", "modal-body");  // створюємо розмітку модального вікна
    modalWindow.append(modalBody);

    // Робота з кнопками 
    const btns = createHTMLElement("div", "btns");

    modalSave.addEventListener("click", () => {
        newRestSaveProductInfo(modalBody, rez)
        hideModalEvent() // закриваємо модальне вікно після збереження
        modalBody.remove()
        showRestoranMenu(JSON.parse(localStorage.restorationBD)) // перегружаємо дані на сторінці після збереження

    });// зберігаємо зміни

    modalClose.addEventListener("click", () => {
        hideModalEvent()
        modalBody.remove()
    });// відміняємо зміни

    btns.append(modalSave, modalClose);
    modalWindow.append(btns)

    //Визначення обєкта для редагування
    const rez = restBD.find((a) => {
        return span.dataset.key === a.id
    }); // rez відповідає за об'єкт в якому span, що нажімається,  має вказаний id
    const data = Object.entries(rez);// перебирання об'єкта 

    // Редагування позиції
    const inputsElemets = data.map(([props, value]) => {
        return createEditProductInput(props, value) // функція для відображення модального вікна для редагування
    })
    modalBody.append(...inputsElemets)
}
function newRestSaveProductInfo(newObj, oldObj) { // нове збереження даних після редагування
    const inputs = newObj.querySelectorAll("input");

    const obj = { // параметри які не змінюються
        id: oldObj.id,
        date: oldObj.date,
        status: false
    }

    inputs.forEach(input => {// записуємо параметри які ми змінюємо
        switch (input.key) {
            case "price": obj.price = input.value;
                return
            case "description": obj.description = input.value;
                return
            case "productimageUrl": obj.productimageUrl = input.value;
                return
            case "productName": obj.productName = input.value;
                return
            case "productQuantity": obj.productQuantity = input.value;
                return
            case "keywords": obj.keywords = input.value.split(",");
                return
            case "productWeiht": obj.productWeiht = input.value;
                return
            case "ingredients": obj.ingredients = input.value.split(",");
                return
        }
    })

    if (obj.productQuantity > 0) {// якщо елемент є (кількість >0) - міняємо статус товару
        obj.status = true;
    } else {
        obj.status = false;
    }
    const rest = JSON.parse(localStorage.restorationBD);
    rest.splice(rest.findIndex(el => el.id === oldObj.id), 1, obj);// findIndex - з якого удаляти, 1 - скільки удаляти, obj - на що заміняти
    localStorage.restorationBD = JSON.stringify(rest);
}

function delRest(e) {
    const arrBD = JSON.parse(localStorage.restorationBD)
    const itemArrDel = arrBD.find((a) => {
        return e.target.dataset.del === a.id

    });
    arrBD.splice(arrBD.findIndex(el => el.id === itemArrDel.id), 1);
    localStorage.restorationBD = JSON.stringify(arrBD);
    console.log(itemArrDel.id);
    showRestoranMenu(arrBD)
}


