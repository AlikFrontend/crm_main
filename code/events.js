import { getLogin, getPassword } from "./var.js";

import { generationId, validate, createInputSring, dateNow, } from "./functions.js";
import { StoreElementCRM, RestoranElementCRM, VideoElementCRM } from "./class.js";


//changeInputEvent Object
const isDisabledBtn = {
    flagLogin: false,
    flagPassword: false
}

function changeInputEvent(e) {
    if (e.target.dataset.type === "login" && validate(new RegExp("^" + getLogin + "$"), e.target.value)) { //перевіряємо на валідність логін
        e.target.classList.remove("error")
        isDisabledBtn.flagLogin = true; //змінюємо флаг для кнопки
    } else if (e.target.dataset.type === "password" && validate(new RegExp("^" + getPassword + "$"), e.target.value)) { //перевіряємо на валідність пасворд
        e.target.classList.remove("error")
        isDisabledBtn.flagPassword = true; //змінюємо флаг для кнопки
    } else {
        e.target.classList.add("error");
        if (e.target.dataset.type === "login") {
            isDisabledBtn.flagLogin = false; //вимикаємо кнопку
        } else if (e.target.dataset.type === "password") {
            isDisabledBtn.flagPassword = false;//вимикаємо кнопку
        }
    }

    if (isDisabledBtn.flagLogin && isDisabledBtn.flagPassword) {
        document.getElementById("disabled").disabled = false;//вмикаємо кнопку
    } else {
        document.getElementById("disabled").disabled = true;// вимикаємо кнопку
    }
}

function userLoginEvent() {
    sessionStorage.isLogin = true; // означає, що юзер залогінився
    document.location = "/"
}

function showModalEvent() {
    const modal = document.querySelector(".container-modal").classList.remove("hide") // показуємо модальне вікно
}

function hideModalEvent() {
    const modal = document.querySelector(".container-modal").classList.add("hide") // скриваємо модальне вікно
}

function changeCategoryEvent(e) { // наповнюємо модальне вікно
    const modal__body = document.querySelector(".modal__body");
    modal__body.innerHTML = "";
    if (e.target.value === "Магазин") {
        modal__body.insertAdjacentHTML("beforeend", `
       <form>
        ${createInputSring("text", "Назва продукту", generationId(), "productName")}
        ${createInputSring("number", "Вартість продукту", generationId(), "porductPrice")}
        ${createInputSring("url", "Картинка продукту", generationId(), "productImage")}
        ${createInputSring("text", "Опис продукту", generationId(), "productDescription")}
        ${createInputSring("text", "Ключеві слова для пошуку. Розділяти комою", generationId(), "keywords")}
       </form>
       `)

    } else if (e.target.value === "Відео хостинг") {
        modal__body.insertAdjacentHTML("beforeend", `
        <form>
         ${createInputSring("text", "Назва відео", generationId(), "name")}
         ${createInputSring("url", "Постер", generationId(), "poster")}
         ${createInputSring("url", "Посилання на відео", generationId(), "url")}
         ${createInputSring("text", "Опис відео", generationId(), "description")}
         ${createInputSring("text", "Ключеві слова для пошуку. Розділяти комою", generationId(), "keywords")}
        </form>
        `)
    } else if (e.target.value === "Ресторан") {
        modal__body.insertAdjacentHTML("beforeend", `
        <form>
         ${createInputSring("text", "Назва Страви", generationId(), "productName")}
         ${createInputSring("text", "Грамовка", generationId(), "productWeiht")}
         ${createInputSring("text", "Склад", generationId(), "ingredients")}
         ${createInputSring("text", "Опис продукту", generationId(), "description")}
         ${createInputSring("text", "Ключеві слова для пошуку. Розділяти комою", generationId(), "keywords")}
         ${createInputSring("number", "Вартість продукту", generationId(), "price")}
         ${createInputSring("url", "Забраження продукту", generationId(), "productimageUrl")}
        </form>
        `)
    }
}

function saveData() {
    try {
        const [isCategory] = document.querySelector("select").selectedOptions;
        const [...inputs] = document.querySelectorAll("form input");
        if (isCategory.value === "Магазин") {// для магазину
            const objStore = {
                productName: "string",
                porductPrice: "number",
                productImage: "string",
                productDescription: "string",
                keywords: "string array",
            };

            inputs.forEach(e => {
                objStore[e.dataset.type] = e.value;
                e.value = ''
            })

            const store = JSON.parse(localStorage.store);// зберігаємо отримані дані в localStorage(store)
            store.push(new StoreElementCRM( // заповнюємо наш обєкт
                objStore.productName,
                objStore.porductPrice,
                objStore.productImage,
                objStore.productDescription,
                undefined,
                objStore.keywords,
                dateNow,
                generationId));

            localStorage.store = JSON.stringify(store);

        }
        if (isCategory.value === "Ресторан") {// для ресторану
            const objRest = {
                productName: "string",
                productWeiht: "string",
                ingredients: "string array",
                price: "number",
                productImage: "string",
                description: "string",
                keywords: "string array",
                productimageUrl: "string",
            };

            inputs.forEach(e => {
                objRest[e.dataset.type] = e.value;
                e.value = ''
            })

            const restorationBD = JSON.parse(localStorage.restorationBD);// зберігаємо отримані дані в localStorage(restorationBD)
            restorationBD.push(new RestoranElementCRM( // заповнюємо наш обєкт
                objRest.productName,
                objRest.price,
                objRest.productWeiht,
                objRest.ingredients,
                objRest.productimageUrl,

                objRest.description,
                undefined,
                objRest.keywords,
                dateNow,
                generationId));

            localStorage.restorationBD = JSON.stringify(restorationBD);

        }
        if (isCategory.value === "Відео хостинг") {// для відеохостингу
            const objVideo = {
                name: "string",
                poster: "string",
                url: "string",
                description: "string",
                keywords: "string array",
            };

            inputs.forEach(e => {
                objVideo[e.dataset.type] = e.value;
                e.value = ''
            })

            const video = JSON.parse(localStorage.video);// зберігаємо отримані дані в localStorage(video)
            video.push(new VideoElementCRM( // заповнюємо наш обєкт
                objVideo.name,
                objVideo.poster,
                objVideo.url,
                objVideo.description,
                objVideo.keywords,
                dateNow,
                generationId));

            localStorage.video = JSON.stringify(video);


        }
    } catch (e) {
        console.error(e)
    }
}
function exportDataEvent() {// експортування даних
    /* // для відображення на цій же сторінці в модальному вікні
    const pre = document.createElement("pre")
    pre.classList.add("text-block")
    const div = document.createElement("div")
    const store = JSON.parse(localStorage.store)
    const video = JSON.parse(localStorage.video)
    const rest = JSON.parse(localStorage.restorationBD)
    div.innerHTML = JSON.stringify([rest, store, video])
    pre.append(div)
    showModalEvent()
    document.querySelector(".modal").append(pre)
    setTimeout(() => {
        hideModalEvent()
    }, 10000)
    */
    let windowData = open("/window") // відображення даних  у новому вікні
    setTimeout(() => {
        windowData.close()
    }, 10000) //закриття вікна через вказаний інтервал
}

export { changeInputEvent, userLoginEvent, showModalEvent, changeCategoryEvent, hideModalEvent, saveData, exportDataEvent }