import { getLogin, getPassword, modalClose, modalSave } from "./var.js";
import { changeInputEvent, userLoginEvent, showModalEvent, hideModalEvent, saveData, exportDataEvent } from "./events.js";
import { categorySelect, req } from "./functions.js";

if (!sessionStorage.isLogin && !document.location.pathname.includes("/authorization")) {
    document.location = "/authorization";
}
//authorization
const exportBtn = document.querySelector("#export")
const reqBtn = document.querySelector("#req")
try {
    document.querySelector(".window form")
        .addEventListener("change", changeInputEvent); //перевіряємо чи вірно введено дані

    document.getElementById("disabled")
        .addEventListener("click", userLoginEvent)
} catch (error) {
    console.error(error)
}

console.log(getLogin);
console.log(getPassword);

// //main page
try {
    document.querySelector(".add")
        .addEventListener("click", showModalEvent); // при нажаті на кнопку "додати щось нове" показуємо модальне вікно

    const modalWindow = document.querySelector(".container-modal .modal");

    modalWindow.insertAdjacentHTML("beforeend", `
    <h2>Додайте новий продукт до БД</h2>
    <div class="catigory"></div>
    <div class="modal__body"></div>
    <div class="modal__control"></div>
    `) // розмітка модального вікна

    document.querySelector(".modal__control").append(modalSave, modalClose); // створення кнопок
    categorySelect(); // створення категорій для додавання

    modalClose.addEventListener("click", hideModalEvent); // ховаємо модальне вікно

    modalSave.addEventListener("click", saveData) // зберігає введені дані

    /*
    Ваші події 
    */

} catch (e) {

}
try {
    exportBtn.addEventListener("click", exportDataEvent) // при нажаті на "експорт"
    reqBtn.addEventListener("click", () => {
        req("fetch", "https://jsonplaceholder.typicode.com/posts")
    })

} catch (e) {
    console.error(e)

}



if (!localStorage.store) {
    localStorage.store = JSON.stringify([]) //якщо немає localStorage, то створюємо новий об'єкт (для магазину)
}
if (!localStorage.video) {
    localStorage.video = JSON.stringify([])//якщо немає localStorage, то створюємо новий об'єкт (для відео)
}
if (!localStorage.restorationBD) {
    localStorage.restorationBD = JSON.stringify([])//якщо немає localStorage, то створюємо новий об'єкт (для ресторану)
}
