import { changeCategoryEvent } from "./events.js";

export const validate = (p, v) => p.test(v); // функція для валідації
export const generationId = () => { // створюємо унікальний ID 
    const sizeID = Math.floor(Math.random() * (10 - 18) + 18)
    const a = "qwertyuiopasdfghjklzxcvbnm1234567890$&";
    let r = "";

    for (let i = 0; i < sizeID; i++) {
        r += a[Math.floor(Math.random() * a.length)]; // Math.floor - округлює значення
    }

    return r
}

export function dateNow() {
    return `${new Date().getFullYear()}/${new Date().getMonth()}/${new Date().getDate()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}` // функція для показу дати
}

export function categorySelect() { // створення категорій в модальному вікні  для додавання 
    const catigory = document.querySelector(".catigory");

    catigory.insertAdjacentHTML("beforeend",
        `<select id="category">
    <option value="" disabled selected>Оберіть категорію</option>
    <option value="Магазин">Магазин</option>
    <option value="Відео хостинг">Відео хостинг</option>
    <option value="Ресторан">Ресторан</option>
</select>`);

    document.querySelector("#category")
        .addEventListener("change", changeCategoryEvent) // cтворюємо слухач на те, що вибрав користувач (категорію)
}


export function createHTMLElement(tagName = "div", className, value, attr = [], listener) {
    const el = document.createElement(tagName); //добавляємо новий елемент
    if (className) {
        el.classList.add(className) // добавляємо клас
    }
    if ("inputtextareaoption".includes(tagName)) {
        if (value) {
            el.value = value; //добавляємо value
        }
        if (listener) {
            el.addEventListener("change", listener) // якщо необхідно виконати якусь дію (change)
        }
    } else {
        if (value !== undefined) {
            el.innerHTML = value;
        }
        if (listener) {
            el.addEventListener("click", listener)// якщо необхідно виконати якусь дію (click)
        }
    }
    attr.forEach(attr => {
        // Зробити, щоб ми отримували точно сам об.
        if (typeof attr === "object" && attr !== null) {
            // отримати Значання з об...
            //el.setAttribute(attr.props, attr.value)
            el.setAttribute(Object.entries(attr)[0][0], Object.entries(attr)[0][1]) // добавляємо атрибутии  
        }
    })
    return el;
}

export function createInputSring(type = "text", value = "", id, key) { // створення шаблону для форми в модальному вікні
    const input = `
    <div class="element-product">
      <label for="${id}">${value}</label>
      <input type="${type}" id="${id}" data-type="${key}">
    </div>
    `
    return input
}

export function createEditProductInput(p, v) {// функція для відображення модального вікна для редагування
    const div = createHTMLElement("div", undefined);
    const id = generationId();
    const label = createHTMLElement("label", undefined, p, [{ for: id }]);
    const input = createHTMLElement("input", undefined, v);
    input.key = p;
    if (p === "status") {
        input.type = "checkbox" // робимо input "status" типу checkbox
    }
    if (p === "id" || p === "date" || p === "status") { // що не можемо змінювати
        input.disabled = true;
    }
    input.id = id;

    div.append(label, input)
    return div
}
export function req(type = "ajax", url = "/") {

    if (type === "ajax") {
        const r = new XMLHttpRequest();
        r.open("GET", url);
        r.send();
        r.addEventListener("readystatechange", () => {
            if (r.readyState === 4 && r.status >= 200 && r.status < 300) {
                localStorage.server = r.responseText
                document.body.classList.add("rdone")
                setTimeout(() => {
                    document.body.classList.remove("rdone")

                }, 2000)
            } else if (r.readyState === 4) {
                throw new Error(`Помилка з запитом: статус код: ${r.status}`)
            }
        })
        r.onerror = () => {
            alert("Немає зв'язку! Перевірте підключення до мережі!")
        }
    } else if (type === "fetch") {
        const info = fetch(url)
        info.then((data) => {
            console.dir(data)
            return data.json()
        })
            .then((infoJson) => {
                document.body.classList.add("rdone")
                setTimeout(() => {
                    document.body.classList.remove("rdone")

                }, 2000)
                localStorage.serverFetch = JSON.stringify(infoJson)
            })
            .catch(e => {
                throw new Error(`Помилка з запитом: статус код: ${e.message}`)
            })
    }

}
