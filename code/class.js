
class StoreElementCRM {
    constructor(productName = "", porductPrice = 0, productImage = "/img/error.png", productDescription = "", productQuantity = 0, keywords = [], dateNow = () => { }, id = () => { }) {
        this.id = id()
        this.date = dateNow()
        this.productName = productName;
        this.porductPrice = porductPrice;
        this.productImage = productImage;
        this.productDescription = productDescription;
        this.productQuantity = productQuantity;
        this.keywords = keywords.split(",");
        this.status = false;
    }

};
class RestoranElementCRM {
    constructor(productName = "", price = 0, productWeiht = 0, ingredients = [], productimageUrl = "/img/error.png", description = "", productQuantity = 0, keywords = [], dateNow = () => { }, id = () => { }) {
        this.id = id()
        this.date = dateNow()
        this.productName = productName;
        this.price = price;
        this.productimageUrl = productimageUrl;
        this.description = description;
        this.productQuantity = productQuantity;
        this.keywords = keywords.split(",");
        this.status = false;
        this.productWeiht = productWeiht;
        this.ingredients = ingredients.split(",")

    }
}

class VideoElementCRM {
    constructor(name = "", poster = "/img/error.png", url = "", description = "", keywords = [], dateNow = () => { }, id = () => { }) {
        this.id = id()
        this.date = dateNow()
        this.name = name;
        this.poster = poster;
        this.description = description;
        this.keywords = keywords.split(",");
        this.url = url;
    }

}

export { StoreElementCRM, RestoranElementCRM, VideoElementCRM }