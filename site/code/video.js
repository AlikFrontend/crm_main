const video = JSON.parse(localStorage.video); // отримуємо дані (в цьому випадку із localStorage)

if (!Array.isArray(video)) {
    throw Error("...") // перевіряємо чи прийшов масив
}

const videoEl = video.map(({ videoName, id, url, description, keywords, poster }) => { // для кожного елемента генеруємо розмітку
    return `
    <div class="video">
    <h3 class="video-name">${videoName}</h3>
    <video id="${id}" controls poster="${poster}">
        ${url.startsWith("/video") ? `<source src="/video/${url}">` : `<source src="${url}">`}
    </video>
    <p class="video-description">
    ${description}
    </p>
    <div>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".video-box")
    .insertAdjacentHTML("beforeend", videoEl.join(""));

